create trigger {nombre}01
  insert on {nombre}
  referencing new as newmov
  for each row
  (execute procedure
    {nombre}_pro(
       1
      ,current year to second
      ,{newmov.campos}
    )
  );

