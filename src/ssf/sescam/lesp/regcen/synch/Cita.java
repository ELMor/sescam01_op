package ssf.sescam.lesp.regcen.synch;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

import ssf.sescam.lesp.regcen.db.RegGen;
import java.util.Date;
import org.apache.log4j.Logger;

/**
 * <p>Title: Recopilacion Listas de Espera SESCAM</p>
 * <p>Description: Sistema de recopilacion de Informix HP-HIS de listas de espera</p>
 * <p>Copyright: Copyright (c) 2002 Soluziona</p>
 * <p>Company: Soluziona</p>
 * @author Eladio Linares
 * @version 1.0
 */

public class Cita extends RegGen {
	Logger log = null;

	public Cita(Connection conn, Properties p) throws Exception {
		super(conn, p, "mst.cita", "rel.cita");
		log = Logger.getLogger(getClass());
	}

	/**
	 * Inserta una fila en lespcex
	 * @param st Statement que se usa para lanzar el comando
	 * @return statusProc ==-2 si hubo un error
	 * @throws Exception
	 */
	public int TriggerInsert(Statement st) throws Exception {
		//Primero borramos el registro que hubiere
		RegisterDelete(st);
		/**
		 * Al cargar el registro, motisali puede contener los siguientes valores
		 * '-R-X-Y' Significa cita realizada donde X=actividad.realizada e Y=actividad.jornada
		 * '-CN'    Significa citada
		 * '-CI'    Significa citada con demora medica
		 * '-CP'    Significa citada con demora voluntaria
		 * '-A-Z'   Significa cita anulada donde Z=anulacion.motivo
		 */
		String motisali = (String) get("motisali");
		String indinoga = (String) get("indinoga");
		if (indinoga != null)
			motisali += indinoga;

/*   

// 	sincronización de motisali con cexincid  
 	
 		if (motisali.equals("-CP")) {
			ResultSet rs =
				st.executeQuery(
					"select count(*) from cexincid where ncita = "
						+ reg.get("ncita"));
			if (rs.next())
				motisali = (rs.getInt(1) > 2) ? "-CPG" : motisali;
			rs.close();
		} else if (motisali.equals("-CN")) {
			ResultSet rs =
				st.executeQuery(
					"select count(*) from cexincid where  tipinci = 'R' and perdida = 'S' and ncita = "
						+ reg.get("ncita"));
			if (rs.next())
				motisali = (rs.getInt(1) > 0) ? "-CR" : motisali;
			rs.close();
		}
*/
		set("motisali", motisali);
		String sqlInsert =
			resolveSQLFields(getProp("sql.insert.sescam.lespcex"));
		return runIt(st, sqlInsert);
	}
	
	/**
	 * Modifica una fila en lespcex (borra y añade)
	 * @param st Statement que se usa para lanzar el comando
	 * @return statusProc ==-2 si hubo un error
	 * @throws Exception
	 */
	public int TriggerUpdate(Statement st) throws Exception {
		return TriggerInsert(st);
	}

	/**
	 * Borra una fila de lespcex
	 * @param st Statement que se usa para lanzar el comando
	 * @return statusProc ==-2 si hubo un error
	 * @throws Exception
	 */
	public int TriggerDelete(Statement st) throws Exception {
		//No se borra, en realidad se reajusa la fecha de salida LEC y motivo
		return TriggerInsert(st);
	}

	public int RegisterDelete(Statement st) {
		String sqlDelete =
			resolveSQLFields(getProp("sql.delete.sescam.lespcex"));
		return runIt(st, sqlDelete);
	}

	/**
	 * Ejecuta un comando sql y devuelve un codigo de error/OK
	 * @param st Statement usado para lanzar el comando
	 * @param sql Sentencia SQL
	 * @return Error (<0) o OK
	 */
	private int runIt(Statement st, String sql) {
		int stProc = 2;
		try {
			st.execute(sql);
		} catch (Exception e) {
			log.error(sql, e);
			stProc = -2;
		}
		return stProc;
	}

	/**
	 * <p>Chequeo condicion de usuario del registro actual de rs.
	 * <p>En la actualidad comprueba que la fecha de cita sea igual o superior a
	 * la que aparece en config.properties como lecexp.fecha.inicio y que el GrupoCex
	 * de la prestación aparezca en lecexp.grupocex.regcen
	 * @param rs Contiene el registro cita actual a comprobar
	 * @param lfi Fecha de inicio
	 * @param grc Grupos de prestaciones
	 * @return True si se debe centralizar
	 * @throws Exception
	 */
	public boolean CheckCond(ResultSet rs, Date lfi, String grc)
		throws Exception {
		//Comprobar que no es anterior la cita a la fecha de inicio
		//String fechacit=rs.getString("fechacit");
		java.sql.Date fechacit = rs.getDate("fechacit");
		if (fechacit.before(lfi))
			return false;
		//Comprobar que es una prestación que se debe centralizar
		String codPrest = rs.getString("prestaci").trim();
		String codGrupo =
			getMasterTables().Results(
				"prestacion",
				"#" + codPrest,
				"identpres");
		if (grc.indexOf("'" + codGrupo + "'") == -1)
			return false;
		return true;
	}

}