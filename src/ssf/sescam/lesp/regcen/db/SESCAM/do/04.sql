create or replace view linklespsum as
select linklesp.*
from lesp,linklesp
where
	  lesp.centro=linklesp.centro
  and lesp.norden=linklesp.norden
  and (lesp.link is null or lesp.link=-lesp.norden)
