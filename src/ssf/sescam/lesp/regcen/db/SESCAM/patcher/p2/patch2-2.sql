Create Or Replace Package legase Is
   Procedure Init_TLE;

   Procedure Refresh_TLE (vcentro Number);

   Procedure Simple (vcentro In Number, vtipo In Number, vclave In Number);

   Procedure Simple_CIP (vcip In Varchar2);

   Procedure simple_solape (
      vcentro    In   Number
     ,vtipo      In   Number
     ,claveini   In   Number
   );

   Procedure simple_continuo (
      vcentro     In   Number
     ,vtipo       In   Number
     ,vclaveini   In   Number
   );

   Function Simple_catalog (
      vcentro   In   Number
     ,vtipo     In   Number
     ,vclave    In   Number
   )
      Return Varchar2;

   Procedure SyncMasterclave (
      vcentro   In   Number
     ,vtipo     In   Number
     ,vclave    In   Number
   );

   Function Clave_Inicial (
      vcentro   In   Number
     ,vtipo     In   Number
     ,vclave    In   Number
   )
      Return Number;

   Type tend_demor Is Table Of Varchar2 (6)
      Index By Binary_integer;
End;
/

Create Or Replace Package Body legase Is
   --Este procedimiento inicializa la tabla masterclave y luego
   -- NO llama a refresh_tle
   Procedure Init_TLE Is
      Cursor c1 Is
         Select Distinct centro, norden
                    From lesp;

      Cursor c2 Is
         Select Distinct cega, ncita
                    From lespcex;

      r1              c1%Rowtype;
      r2              c2%Rowtype;
      commitcounter   Number       := 0;
   Begin
      For r1 In c1 Loop
         syncmasterclave (r1.centro, 1, r1.norden);
         commitcounter    := commitcounter + 1;

         If commitcounter > 500 Then
            Commit;
            commitcounter    := 0;
         End If;
      End Loop;

      Commit;

      For r2 In c2 Loop
         syncmasterclave (r2.cega, 2, r2.ncita);
         commitcounter    := commitcounter + 1;

         If commitcounter > 500 Then
            Commit;
            commitcounter    := 0;
         End If;
      End Loop;

      Commit;
   End init_tle;

   --Este procedimiento recorre masterclave y para cada clave llama a simple
   Procedure Refresh_TLE (vcentro Number) Is
      Cursor c Is
         Select centro, tipo, clave
           From masterclave
          Where estado = 1 And centro = vcentro;

      rc              c%Rowtype;
      commitCounter   Number      := 0;
   Begin
      For rc In c Loop
         simple (rc.centro, rc.tipo, rc.clave);
         commitCounter    := commitCounter + 1;

         Update masterclave
            Set estado = 0
          Where centro = rc.centro And tipo = rc.tipo And clave = rc.clave;

         If commitCounter > 99 Then
            Commit;
            commitCounter    := 0;
         End If;
      End Loop;

      Commit;
   End refresh_tle;

   --Este procedimiento cataloga las entradas del registro de pacientes

   Function simple_catalog (vcentro Number, vtipo Number, vclave Number)
      Return Varchar2 Is
      codpr      Varchar2 (24);
      exist      Number;
      suma       Number;
      fecha      Date;
      servi      Varchar2 (128);
      activo     Number;
      garant     Number;
      rechazos   Number;
	  cursor     map1(cpr varchar2,vc number ,srv varchar2) is          
	  Select g.SESESP_COD,g.proc_garantia, p.proc_activo
           From ses_procedimientos p
               ,ses_proc_map mp
               ,ses_proc_garantia g
               ,ses_servicios s
               ,ses_srv_map sm
          Where trim(mp.proc_cen_cod) = cpr
            And mp.centro = vc
            And (g.SESESP_COD = s.SESESP_COD Or g.SESESP_COD Is Null)
            And p.proc_pk = mp.proc_pk
            And g.PROC_PK = p.PROC_PK
            And trim(sm.SRV_CEN_COD) = srv
            And trim(s.SESSRV_COD) = trim(sm.SESSRV_COD)
            And sm.CENTRO = vc
		  order by 1;
 	 map1reg	map1%rowtype;
	 
   Begin
      If vtipo = 1 Then
         --Comprobamos que no tiene fecha de salida.
         Select activo
           Into exist
           From oltle
          Where centro = vcentro And tipo = 1 And claveinicial = vclave;

         If exist = 0 Then
            Return 'HISTOR';
         End If;

         --Si cpr1 es nulo, el procedimiento no est� grantizado
         Select trim(cpr1), trim(secc)
           Into codpr, servi
           From lesp
          Where centro = vcentro And norden = vclave;

         If codpr Is Null Then
            Return 'PSG';
         End If;

         --Comprobacion de mapeos
         Select Count (*)
           Into exist
           From ses_procedimientos p
               ,ses_proc_map mp
               ,ses_proc_garantia g
               ,ses_servicios s
               ,ses_srv_map sm
          Where trim(mp.proc_cen_cod) = codpr
            And mp.centro = vcentro
            And (g.SESESP_COD = s.SESESP_COD Or g.SESESP_COD Is Null)
            And p.proc_pk = mp.proc_pk
            And g.PROC_PK = p.PROC_PK
            And trim(sm.SRV_CEN_COD) = servi
            And trim(s.SESSRV_COD) = trim(sm.SESSRV_COD)
            And sm.CENTRO = vcentro;

         If exist = 0 Then
            Return 'NOMAP';
         End If;

         -- Si spac=9 entonces es SG-RO
         Select spac
           Into exist
           From lesp
          Where centro = vcentro And norden = vclave;

         If exist = 9 Then
            Return 'SG-RO';
         End If;

         -- Si hay mas de 2 demoras voluntarias, 'SG-BPA'
         Select Count (*)
           Into exist
           From lehpdemo
          Where centro = vcentro And norden = vclave And tipo = 4;

         If exist > 2 Then
            Return 'SG-BPA';
         End If;

		 --Comprobamos garantia.
		 open map1(codpr,vcentro,servi);
		 fetch map1 into map1reg;
		 close map1;
		 garant:=map1reg.proc_garantia;
		 activo:=map1reg.proc_activo;
		 
         If garant = 0 Or activo = 0 Then
            Return 'PSG';
         End If;

         Select Sum (ndias)
           Into suma
           From lehpdemo
          Where centro = vcentro And norden = vclave And tipo = 4;

         If suma > garant Then
            Return 'SG-BPA';
         End If;

         Select dem
           Into codpr
           From oltle
          Where centro = vcentro And tipo = 1 And claveinicial = vclave;

         If codpr Is Null Then
            Return 'TG';
         End If;

         Return 'TG-' || codpr;
      Else -- Catalogacion de LESPCEX.

           --Es posible que tenga fecha de salida.
         Select activo, dem
           Into exist, codpr
           From oltle
          Where centro = vcentro And tipo = 2 And claveinicial = vclave;

         If exist = 0 And codpr Is Null Then
            Return 'HISTOR';
         End If;

         --Si prestaci es nulo, el procedimiento no est� grantizado
         Select trim(prestaci), trim(servreal)
           Into codpr, servi
           From lespcex
          Where cega = vcentro And ncita = vclave;

         If codpr Is Null Then
            Return 'PSG';
         End If;

         -- Puede que no este mapeado (LESPCEX) 

         Select Count (*)
           Into exist
           From ses_procedimientos p
               ,ses_proc_map mp
               ,ses_proc_garantia g
               ,ses_servicios s
               ,ses_srv_map sm
          Where trim(mp.proc_cen_cod) = codpr
            And mp.centro = vcentro
            And (g.SESESP_COD = s.SESESP_COD Or g.SESESP_COD Is Null)
            And p.proc_pk = mp.proc_pk
            And g.PROC_PK = p.PROC_PK
            And trim(sm.SRV_CEN_COD) = servi
            And trim(s.SESSRV_COD) = trim(sm.SESSRV_COD)
            And sm.CENTRO = vcentro;

         If exist = 0 Then
            Return 'NOMAP';
         End If;

         -- Si hay mas de 2 demoras voluntarias, 'SG-BPA' (LESPCEX).
         Select Count (*)
           Into exist
           From cexincid c, lespcex l
          Where c.centro = vcentro
            And c.centro = l.cega
            And l.ncita = vclave
            And c.ncita = l.ncita
            And l.motisali = '-CP';

         If exist > 2 Then
            Return 'SG-BPA';
         End If;

		 open map1(codpr,vcentro,servi);
		 fetch map1 into map1reg;
		 close map1;
		 garant:=map1reg.proc_garantia;
		 activo:=map1reg.proc_activo;

         If activo = 0 Or garant = 0 Then
            Return 'PSG';
         End If;

         Select Sum (dias_demo)
           Into suma
           From cexincid c, lespcex l
          Where c.centro = vcentro
            And l.cega = vcentro
            And c.ncita = vclave
            And l.ncita = vclave
            And l.motisali = '-CP';

         If suma > garant Then
            Return 'SG-BPA';
         End If;

		 --Comprobar los rechazos en CEXINCID.
         Select Count (*)
           Into rechazos
           From cexincid
          Where centro = vcentro
            And ncita = vclave
            And tipinci = 'R'
            And perdida = 'S';

         If rechazos > 0 Then
            Return 'SG-RO';
         End If;

         Select dem
           Into codpr
           From oltle
          Where centro = vcentro And tipo = 2 And claveinicial = vclave;

         If codpr Is Null Then
            Return 'TG';
         End If;

         Return 'TG-' || codpr;
      End If;
   End simple_catalog;

   Procedure simple_solape (
      vcentro    In   Number
     ,vtipo      In   Number
     ,claveini   In   Number
   ) Is
      Cursor c_cexincid (vcentro Number, vncita Number) Is
         Select *
           From cexincid
          Where centro = vcentro And ncita = vncita And tipinci = 'A';

      r_cexincid   c_cexincid%Rowtype;
      fectopdemo   Date;
      feciniq      Date;
      fecendq      Date;
      rlespcex     lespcex%Rowtype;
   Begin
       --Encontramos la claveinicial. Si es quirurgica (tipo=1) hay que recorrer un arbol
      --si es consulta o prueba (tipo=2) entonces claveinicial=clave
      If vtipo = 2 Then
         Delete From solapes
               Where claveinicial = claveini And tipo = 2
                     And centro = vcentro;

         Delete From continuos
               Where claveinicial = claveini And tipo = 2
                     And centro = vcentro;

         --Insertamos valores positivos procedentes de consultas y tecnicas
         --se transforman los motivos '-4' y '-5' a 4 y 5 respectivamente para
         --que sean consecuentes con los de quirurgica
         Select cega, ncita, finclusi
               ,fsalida, fechacit
           Into rlespcex.cega, rlespcex.ncita, rlespcex.finclusi
               ,rlespcex.fsalida, rlespcex.fechacit
           From lespcex
          Where cega = vcentro And ncita = claveini;

         --Como quedamos, si fsalida es nula pero la fecha de cita ya ha pasado
         --a dia de hoy, suponer que no se ha capturado actividad en el centro
         --y ponerlo como realizado.
         If rlespcex.fsalida Is Null Then
            --Temporalmente, marcar como realizada.
            If Sysdate > rlespcex.fechacit Then
               --Algunas entradas se introducen DESPUES de la cita,
               --Corregimos para no obtener TLEs negativos.
               If rlespcex.finclusi > rlespcex.fechacit Then
                  rlespcex.finclusi    := rlespcex.fechacit;
               End If;

               rlespcex.fsalida    := rlespcex.fechacit;
            End If;
         End If;

         Insert Into solapes
                     (centro, clave, claveinicial, tipo, signo
                     ,fi, ff, situ
                     )
              Values (rlespcex.cega, rlespcex.ncita, rlespcex.ncita, 2, 1
                     ,rlespcex.finclusi, rlespcex.fsalida, 0
                     );

         --Valores negativos de consultas y tecnicas, procedentes de cexincid.
         --Por convencion se toman los tramos de demora uno a continuacion de otro
         --y se colocan en el inicio de inclusion en lista de espera.
         fectopdemo    := Null;

         For r_cexincid In c_cexincid (vcentro, claveini) Loop
            If fectopdemo Is Null Then
               fectopdemo    := r_cexincid.fec_inci;
            End If;

            Insert Into solapes
                        (centro, clave, claveinicial, tipo, signo, fi
                        ,ff, situ
                        )
                 Values (vcentro, claveini, claveini, 2, -1, fectopdemo
                        , fectopdemo + r_cexincid.dias_demo, 4
                        );

            fectopdemo    := fectopdemo + r_cexincid.dias_demo;
         End Loop;
      Else --Es Quirurgica. Borramos primero lo que haya
         Delete From solapes
               Where claveinicial = claveini And tipo = 1
                     And centro = vcentro;

         Delete From continuos
               Where claveinicial = claveini And tipo = 1
                     And centro = vcentro;

           --Insertamos valores positivos procedentes de quirurgica
         --Es posible insertar 2 veces el mismo tramo positivo. Es necesario
         --en el caso de registros unidos colocar el min(finc) como finc y el
         --max(fsal) como fsal para registros historicos o NULL para registros
         --activos.
         Select     Min (finchos)
                   ,Max (Nvl (fsal, To_date ('01-01-3000', 'DD-MM-YYYY')))
               Into FecIniQ
                   ,FecEndQ
               From lesp
         Start With norden = claveini And centro = vcentro
         Connect By Prior Link = norden And Prior centro = centro;

         If FecEndQ = To_date ('01-01-3000', 'DD-MM-YYYY') Then
            FecEndQ    := Null;
         End If;

         Insert Into solapes
                     (centro, clave, claveinicial, tipo, signo, fi, ff, situ
                     )
              Values (vcentro, claveini, claveini, 1, 1, feciniq, fecendq, 0
                     );

         --Valores negativos de quirurgica
         Insert Into solapes
                     (centro, clave, claveinicial, tipo, signo, fi, ff, situ)
            Select centro, norden, claveini, 1, -1, fec_inicio
                  , fec_inicio + ndias, tipo
              From lehpdemo
             Where centro = vcentro
               And tipo In (4, 5)
               And norden In (
                             Select     norden
                                   From lesp
                             Start With centro = vcentro And norden = claveini
                             Connect By centro = vcentro
                                        And Prior Link = norden);
      End If;
   End simple_solape;

   Function takeCat (k tend_demor, m Number)
      Return Varchar2 Is
      i   Number;
   Begin
      i    := m;

      If m < 1 Then
         Return Null;
      End If;

      Loop
         If k (i) Is Not Null Then
            Return k (i);
         End If;

         i    := i - 1;

         If i < 1 Then
            Exit;
         End If;
      End Loop;

      Return Null;
   End takeCat;

   Procedure simple_continuo (
      vcentro     In   Number
     ,vtipo       In   Number
     ,vclaveini   In   Number
   ) Is
      Cursor c (ce Number, ti Number, clini Number) Is
         Select signo, fi, ff, claveinicial, clave, situ
           From solapes
          Where centro = ce And tipo = ti And claveinicial = clini;

      Cursor c2 (ce Number, ti Number, clini Number) Is
         Select        signo, fi, spac
                  From continuos
                 Where centro = ce And tipo = ti And claveinicial = clini
              Order By fi Asc
         For Update Of signo, ff, spac, acu;

      Cursor c3 (ce Number, ti Number, clini Number) Is
         Select   fi
             From continuos
            Where centro = ce And tipo = ti And claveinicial = clini
         Order By fi Asc;

      rc              c%Rowtype;
      rc2             c2%Rowtype;
      rc3             c3%Rowtype;
      contador        Number;
      cont            Number;
      ultcont         Number;
      sigcont         Number;
      acumulador      Number       := 0;
      demora          Varchar2 (6);

      Type tend_dates Is Table Of Date
         Index By Binary_integer;

      end_dates       tend_dates;
      end_demor       tend_demor;
      solCounter      Number       := 0;
      demorCounter    Number       := 0;
      mDemorCounter   Number       := 0;
      acuDias         Number       := 0;
      endDate         Date;
   Begin
      Delete From continuos
            Where centro = vcentro
              And tipo = vtipo
              And claveinicial = vclaveini;

      For rc In c (vcentro, vtipo, vclaveini) Loop
         solCounter    := solCounter + 1;

         Insert Into continuos
                     (centro, tipo, claveinicial, clave, signo
                     ,fi, spac
                     )
              Values (vcentro, vtipo, rc.claveinicial, rc.clave, rc.signo
                     ,rc.fi, rc.situ || ',' || solCounter
                     );

         Insert Into continuos
                     (centro, tipo, claveinicial, clave, signo
                     ,fi, spac
                     )
              Values (vcentro, vtipo, rc.claveinicial, rc.clave, -rc.signo
                     ,rc.ff, -rc.situ || ',' || solCounter
                     );
      End Loop;

      contador    := 0;

      For rc3 In c3 (vcentro, vtipo, vclaveini) Loop
         end_dates (contador + 1)    := rc3.fi;
         contador                    := contador + 1;
         end_demor (contador)        := Null;
      End Loop;

      cont        := 1;
      ultcont     := 1;

      If contador = 1 Then
         sigcont    := 1;
      Else
         sigcont    := 2;
      End If;

      For rc2 In c2 (vcentro, vtipo, vclaveini) Loop
         acumulador    := acumulador + rc2.signo;

         If rc2.spac Like '-%' Then
            demorCounter                := To_number (Substr (rc2.spac, 4));
            end_demor (demorCounter)    := Null;
         Else
            demorCounter    := To_number (Substr (rc2.spac, 3));

            If rc2.spac Like '4%' Then
               end_demor (demorCounter)    := 'DV';
            Elsif rc2.spac Like '5%' Then
               end_demor (demorCounter)    := 'DM';
            Else
               end_demor (demorCounter)    := Null;
            End If;
         End If;

         If demorCounter > mDemorCounter Then
            mDemorCounter    := demorCounter;
         End If;

         demora        := takeCat (end_demor, mDemorCounter);

         If acumulador > 0 And end_dates (sigcont) Is Not Null Then
            acuDias    := acuDias + (end_dates (sigcont) - rc2.fi);
         End If;

         If cont = contador Then
            endDate    := Null;
         Else
            endDate    := end_dates (sigcont);
         End If;

         Update continuos
            Set signo = acumulador
               ,ff = endDate
               ,spac = demora
               ,acu = acuDias
          Where Current Of c2;

         ultcont       := cont;
         cont          := cont + 1;

         If cont + 1 > contador Then
            sigcont    := cont;
         Else
            sigcont    := cont + 1;
         End If;
      End Loop;

      Delete From continuos
            Where centro = vcentro
              And tipo = vtipo
              And clave = vclaveini
              And fi Is Null
              And ff Is Null;
   End simple_continuo;

   --Refresca completamente una clave.
   Procedure Simple (vcentro In Number, vtipo In Number, vclave In Number) Is
      vclaveinicial   Number;
      cata            Varchar2 (6);
   Begin
      vclaveinicial    := clave_inicial (vcentro, vtipo, vclave);
      simple_solape (vcentro, vtipo, vclaveinicial);
      simple_continuo (vcentro, vtipo, vclaveinicial);
      cata             := simple_catalog (vcentro, vtipo, vclaveinicial);

      If vtipo = 1 Then
         Update lesp
            Set catalog = cata
          Where centro = vcentro And norden = vclave;
      Else
         Update lespcex
            Set catalog = cata
          Where cega = vcentro And ncita = vclave;
      End If;
   End simple; --Refresco diferencial de TLE

   Procedure Simple_CIP (vcip In Varchar2) Is
      Cursor c1 (estecip Varchar) Is
         Select centro, norden
           From lesp
          Where cip = estecip;

      Cursor c2 (estecip Varchar) Is
         Select cega, ncita
           From lespcex
          Where cip = estecip;

      r1   c1%Rowtype;
      r2   c2%Rowtype;
   Begin
      For r1 In c1 (vcip) Loop
         simple (r1.centro, 1, r1.norden);
      End Loop;

      For r2 In c2 (vcip) Loop
         simple (r2.cega, 2, r2.ncita);
      End Loop;

      Commit;
   End Simple_CIP;

   Procedure syncMasterclave (
      vcentro   In   Number
     ,vtipo     In   Number
     ,vclave    In   Number
   ) Is
      exist   Number := 0;
   Begin
      Select Count (*)
        Into exist
        From masterclave
       Where centro = vcentro And tipo = vtipo And clave = vclave;

      If exist > 0 Then
         Update masterclave
            Set estado = 1
          Where centro = vcentro And tipo = vtipo And clave = vclave;
      Else
         Insert Into masterclave
                     (centro, tipo, clave, estado
                     )
              Values (vcentro, vtipo, vclave, 1
                     );
      End If;
   End syncmasterclave;

   Function clave_inicial (vcentro In Number, vtipo In Number, vclave In Number)
      Return Number Is
      cini   Number := 0;
   Begin
      If vtipo = 2 Then
         cini    := vclave;
      Else
         Select     Min (norden)
               Into cini
               From lesp
         Start With norden = vclave And centro = vcentro
         Connect By Prior norden = Link And Prior centro = centro;
      End If;

      Return cini;
   End clave_inicial;
End legase;
/