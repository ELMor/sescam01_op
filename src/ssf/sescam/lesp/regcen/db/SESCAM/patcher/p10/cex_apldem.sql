CREATE TABLE CEX_APLDEM
(
  CENTRO      INTEGER                           NOT NULL,
  NCITA       INTEGER                           NOT NULL,
  FECHA_CITA  DATE                              NOT NULL,
  HORA_CITA   CHAR(5)                           NOT NULL,
  TIPO        CHAR(1)							NOT NULL,
  FECHA_INI   DATE,
  FECHA_FIN   DATE
)
/
