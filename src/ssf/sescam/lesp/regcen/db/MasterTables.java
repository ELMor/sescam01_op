package ssf.sescam.lesp.regcen.db;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.Hashtable;
import java.util.Vector;
import java.sql.ResultSetMetaData;
import java.util.Properties;

/**
 * <p>Title: Recopilacion Listas de Espera SESCAM</p>
 * <p>Description:  Gesti�n de las tablas maestras del centro </p>
 * <p>Copyright: Copyright (c) 2002 Soluziona</p>
 * <p>Company: Soluziona</p>
 * @author Eladio Linares
 * @version 1.0
 */

public class MasterTables {
  private Hashtable allTables=new Hashtable();
  private Hashtable lnkTabDes=new Hashtable();
  private String[]   tableNames;
  private String[][] tablePK;
  private String[][] tableDes;

  /**
   * Carga en un Hashtable en local los contenidos de una tabla para realizar
   * joining local. En teor�a m�s r�pido debido al menor tr�fico de red.
   * @param conn Conexi�n a BBDD a usar.
   * @param p de aqui se han de obtener la definicion de las tablas
   * @param head directorio dentro de 'p' donde se haya la definicion
   */
  public MasterTables(Connection conn,
                      Properties p,
                      String head) {
    int numTablas=Integer.parseInt(p.getProperty(head+".tablas"));
    //Lectura de los nombres de las tablas
    //Dimensionamos
    tableNames=new String[numTablas];
    tablePK=new String[numTablas][];
    tableDes=new String[numTablas][];
    //Bucle 'para cada tabla'
    for(int i=1;i<=numTablas;i++){
      //Nombre
      tableNames[i-1]=p.getProperty(head+".tabla."+i);
      //Determinar el numero de campos que componen el PK
      int j=1;
      for(;j<1000;j++)
        if(p.getProperty(head+"."+tableNames[i-1]+".pk."+j)==null)
          break;
      //Dimensionar a j-1 campos
      tablePK[i-1]=new String[j-1];
      //Lectura de los nombre de los campos del PK
      for(int k=1;k<j;k++)
        tablePK[i-1][k-1]=p.getProperty(head+"."+tableNames[i-1]+".pk."+k);
      //Determinar el numero de campos designacion (des) a leer
      for(j=1;j<1000;j++)
        if(p.getProperty(head+"."+tableNames[i-1]+".des."+j)==null)
          break;
      //Dimensionar a j-1 campos
      tableDes[i-1]=new String[j-1];
      //Leerlos
      for(int k=1;k<j;k++)
        tableDes[i-1][k-1]=p.getProperty(head+"."+tableNames[i-1]+".des."+k);
    }

    //Carga de las tablas
    for(int i=0;i<tableNames.length;i++){
      lnkTabDes.put(tableNames[i],tableDes[i]);
      try {
        Load(conn,tableNames[i],tablePK[i],tableDes[i]);
      }catch(Exception e){
        e.printStackTrace();
      }
    }
  }
  /**
   * Carga en una Hashtable tablas peque�as para evitar trafico de red
   * @param c Conexion a la BBDD
   * @param tname Nombre de la tabla
   * @param pk Conjunto de PK
   * @param des Conjunto de descripciones
   * @throws Exception
   */
  public void Load(Connection c, String tname, String[] pk, String[] des)
  throws Exception{
    //Crea la select
    String sql="select";
    for(int i=0;i<pk.length;i++)      sql+=(i==0?" ":",")+pk[i];
    for(int i=0;i<des.length;i++)     sql+=","+des[i];
    sql+=" from "+tname;
    //Colocaremos los datos aqui
    Hashtable ht=new Hashtable();
    Statement st=c.createStatement();
    ResultSet rs=st.executeQuery(sql);
    ResultSetMetaData rsmd=rs.getMetaData();
    //Sustituimos los nombres de los campos segun MetaData
    for(int i=pk.length+1;i<=rsmd.getColumnCount();i++)
      des[i-pk.length-1]=rsmd.getColumnName(i);
    while(rs.next()){
      String pkv="";
      for(int i=0;i<pk.length;i++)
        pkv+="#"+rs.getString(i+1).trim();
      Vector v=new Vector();
      for(int i=0;i<des.length;i++){
        String value=rs.getString(pk.length+i+1);
        if(rs.wasNull())
          value="";
        v.add( value.trim());
      }
      ht.put(pkv,v);
    }
    rs.close();
    st.close();
    allTables.put(tname,ht);
  }

  /**
   * Devuelve un vector con los valores ordenados seg�n tableDes
   * @param table Nombre de la tabla
   * @param pk PK, en formato '#pknum1#pknum2...', con un '#' al inicio de cada
   * campo
   * @param fieldName Nombre del campo que se quiere leer
   * @return Vector con los valores de tableDes
   */
  public String Results(String table, String pk, String fieldName){
    Hashtable htTable=(Hashtable)allTables.get(table);
    if(htTable==null)
      return null;
    Vector v=(Vector)htTable.get(pk);
    if(v==null)
      return null;
    String[] fieldNames=(String[])lnkTabDes.get(table);
    int i;
    for(i=0;i<fieldNames.length;i++)
      if(fieldNames[i].equalsIgnoreCase(fieldName))
         return (String)v.elementAt(i);
    return null;
  }

  /**
   * Devuelve los nombres de los campos Descripcion
   * @param table Nombre de la tabla
   * @return Nombre de los campos descripcion
   */
  public String[] ResultsNames(String table){
    int i;
    for(i=0;i<tableNames.length;i++)
      if(tableNames[i].equalsIgnoreCase(table))
        break;
    if(i<tableNames.length)
      return tableDes[i];
    return null;
  }
}