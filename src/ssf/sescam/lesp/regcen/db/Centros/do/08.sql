
Create view vlesp(
	  norden     ,fecope
   ,cip        ,cega       ,hicl       ,cias
   ,ape1       ,ape2       ,nomb
   ,fnac       ,sexo
   ,domi       ,pobl       ,cpos       ,prov       ,tel1       ,tel2       ,tel3
   ,slab       ,fina       ,gara       ,serv       ,secc       ,acli       ,cmed
   ,apno_med   ,finc       ,cdi1       ,ddi1       ,cdi2       ,ddi2       ,cpr1
   ,dpr1       ,cpr2       ,dpr2       ,tcir       ,tane       ,prio       ,cinc
   ,preo       ,frep       ,fcap       ,spac       ,fder       ,dder       ,fsde
   ,tdes       ,fqui       ,fsal       ,moti       ,obse	   ,motrechazo ,fecrechazo
   ,link
      ) as
  Select
      l.norden                  ,
      ll.fecope                 ,
      p.numtis                  ,
      kk.n                      ,
      l.numerohc                ,
      p.cias                    ,
      p.apellid1                ,
      p.apellid2                ,
      p.nombre                  ,
      p.fechanac                ,
      dsx.descsexo              ,
      p.domiresi                ,
      pobl.nompobla             ,
      p.codipost                ,
      prov.descprov             ,
      p.telefono                ,
      p.telecont                ,
      p.telefono2               ,
      l.bajalabor               ,
      gc.desccas                ,
      l.financia                ,
      s.maes_serv               ,
      l.servreal                ,
      -1                        ,
      l.cod_medico              ,
      trim(apellido1)||
        ' '||
        trim(apellido2)||
        ' '||
        trim(nomb)              ,
      l.fecha_en_lista          ,
      l.codidiag_ingreso        ,
      l.descdiag                ,
      l.codidiag2               ,
      l.descdiag2               ,
      l.codiproc_ingreso        ,
      l.descproc                ,
      l.codiproc2               ,
      l.descproc2               ,
      lh.lambul                 ,
      l.tipoanest               ,
      l.prioridad               ,
      l.inclusion               ,
      l.numpreop                ,
      l.fecapto                 ,
      l.feccaduca               ,
      l.situacion               ,
      l.fecderiva               ,
      l.hospderiva              ,
      l.fecdemora               ,
      l.tpodemora               ,
      l.fechaqui                ,
      kk.d                      ,
      kk.n                      ,
      l.observaciones           ,
      l.motrechazo				,
      l.fecrechazo				,
      kk.n
From
      pacientes   p,
      lista_espe  l,
      lesplogger  ll,
      persclin    pc,
      servicios   s,
      provincias  prov,
      poblacion   pobl,
      des_sexo    dsx,
      grupocas    gc,
      leactihis    lh,
      kk
Where
      p.numerohc=l.numerohc
  and ll.procesada=1
  and ll.norden=l.norden
  and pc.codpers=l.cod_medico
  and l.servreal=s.codserv
  and p.provresi=prov.codprov
  and p.provresi=pobl.codiprov
  and p.poblares=pobl.codpobla
  and p.sexo=dsx.codsexo
  and l.tipfinancia=gc.codcas
  and l.tipoacti=lh.tipoacti