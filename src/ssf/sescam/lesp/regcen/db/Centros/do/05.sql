create trigger le01
	delete on lista_espe
	referencing old as oldmov
  for each row
  (execute procedure
  	logtolog(
  	   10
  	  ,current year to second
      ,oldmov.numerohc
      ,oldmov.servreal
      ,oldmov.cod_medico
      ,oldmov.fecha_en_lista
      ,oldmov.codidiag_ingreso
      ,oldmov.codiproc_ingreso
      ,oldmov.prioridad
      ,oldmov.servpeti
      ,oldmov.medipeti
      ,oldmov.aviso_urg
      ,oldmov.preingresado
      ,oldmov.fecha_ingreso
      ,oldmov.observaciones
      ,oldmov.tipfinancia
      ,oldmov.financia
      ,oldmov.ambito
      ,oldmov.descproc
      ,oldmov.descdiag
      ,oldmov.tipoanest
      ,oldmov.norden
      ,oldmov.tipolis
      ,oldmov.procedede
      ,oldmov.fechaqui
      ,oldmov.numproce
      ,oldmov.hemoterapia
      ,oldmov.complicacion
      ,oldmov.inclusion
      ,oldmov.codidiag2
      ,oldmov.descdiag2
      ,oldmov.ingresado
      ,oldmov.bajalabor
      ,oldmov.numpreop
      ,oldmov.fecrechazo
      ,oldmov.tipoacti
      ,oldmov.fec_prevista
      ,oldmov.hora_prevista
      ,oldmov.codiproc2
      ,oldmov.descproc2
      ,oldmov.derivable
      ,oldmov.situacion
      ,oldmov.est_preop
      ,oldmov.aptociru
      ,oldmov.fecapto
      ,oldmov.feccaduca
      ,oldmov.asa
      ,oldmov.localizado
      ,oldmov.fecdemora
      ,oldmov.tpodemora
      ,oldmov.fecderiva
      ,oldmov.hospderiva
      ,oldmov.fecconfirma
      ,oldmov.grabauser
      ,oldmov.grabadia
      ,oldmov.grabahora
      ,oldmov.motrechazo
      ,null
    )
  );

