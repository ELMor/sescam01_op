create procedure pacientes_log_pro 
(  	
		numerohc    	integer
		,nombre       	nchar(20)
		,apellid1     	char(20)
		,apellid2     	char(20)
		,nomb_fonet   	char(20)
		,ape1_fonet   	char(20)
		,ape2_fonet   	char(20)
		,numtis_antiguo 		char(30)
		,numtis_nuevo   	char(30)
)

               if numtis_antiguo <> numtis_nuevo  then
		insert into pacientes_log 
		  	( 	numerohc
		  		,fecope
		  		,nombre
		  		,apellid1
		  		,apellid2
		  		,nomb_fonet
		  		,ape1_fonet
		  		,ape2_fonet
		  		,numtis_antiguo
		  		,numtis_nuevo
			) values (
				numerohc
		  		,current year to second
		  		,nombre
		  		,apellid1
		  		,apellid2
		  		,nomb_fonet
		  		,ape1_fonet
		  		,ape2_fonet
		  		,numtis_antiguo
		  		,numtis_nuevo
			);
                 end if;

                 call monitorpacientes(numerohc);



end procedure;
/
