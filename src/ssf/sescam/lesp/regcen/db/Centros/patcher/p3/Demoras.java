package ssf.sescam.lesp.regcen.db.Centros.patcher.p3;

import ssf.sescam.lesp.regcen.db.patcher.*;
import java.util.Hashtable;
import java.sql.Connection;
import org.apache.log4j.Logger;
import java.sql.SQLException;
import ssf.utils.Global;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.PreparedStatement;


/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 Soluziona</p>
 * <p>Company: Soluziona</p>
 * @author not attributable
 * @version 1.0
 */

public class Demoras extends ClassPatcher {
  public boolean isDoubleConnectionRequired() {
    return true;
  }
  public boolean patchDoubleConnection(Hashtable params) {
    int CEGA = 0;
    Connection toSescam = null;
    Connection toCentro = null;

    CEGA = ( (Integer) params.get("CEGA")).intValue();
    toSescam = (Connection) params.get("toSescam");
    toCentro = (Connection) params.get("toCentro");
    Logger log=Logger.getLogger(getClass());
    Statement stCentro=null;
    PreparedStatement pstSescam=null;
    ResultSet rsCentro=null;
    try {
      //Drop y Create de la vista rcdcexp
      String drop="drop view rcdcexp";
      String crea=Global.getContentOfFile("ssf/sescam/lesp/regcen/db/Centros/do/17.sql");
      stCentro=toCentro.createStatement();
      String update=
          "Update lespcex set motisali=? where cega="+CEGA+" and ncita=?";
      pstSescam=toSescam.prepareStatement(update);
      stCentro.execute(drop);
      stCentro.execute(crea);
      //Ahora un loop sobre la vista para aquellos registros
      //que ahora tengan motisali=('-CI','-CP')
      String select="Select ncita,motisali from rcdcexp where motisali in ('-CI','-CP')";
      rsCentro=stCentro.executeQuery(select);
      while(rsCentro.next()){
        String kVal=rsCentro.getString(2).equals("-CI")?"10":"9";
        pstSescam.setString(1,kVal);
        pstSescam.setInt(2,rsCentro.getInt(1));
        pstSescam.execute();
      }
    }
    catch (Exception ex) {
      log.error("Parche 3: Mientras se actualizaba el campo spac...",ex);
      return false;
    }

    return true;
  }
}