drop trigger pa01;
/

create trigger pa01
  update on pacientes
  referencing new as newmov old as oldmov
  for each row
  (
   
	execute procedure pacientes_log_pro(
 		newmov.numerohc
 		,newmov.nombre
 		,newmov.apellid1
		,newmov.apellid2
		,newmov.nomb_fonet
		,newmov.ape1_fonet
		,newmov.ape2_fonet
		,oldmov.numtis
		,newmov.numtis
	) 
			
  );
/

create trigger pa02
  insert on pacientes
  referencing new as newmov
  for each row
  (
  insert into pacientes_log 
  	( 	numerohc
  		,fecope
  		,nombre
  		,apellid1
  		,apellid2
  		,nomb_fonet
  		,ape1_fonet
  		,ape2_fonet
  		,numtis_antiguo
  		,numtis_nuevo
	) values (
		newmov.numerohc
		,current year to second
		,newmov.nombre
		,newmov.apellid1
		,newmov.apellid2
		,newmov.nomb_fonet
		,newmov.ape1_fonet
		,newmov.ape2_fonet
		,''
		,newmov.numtis
	)
  );
/

create trigger pa03
  delete on pacientes
  referencing old as oldmov
  for each row
  (
 insert into pacientes_log 
  	( 	numerohc
  		,fecope
  		,nombre
  		,apellid1
  		,apellid2
  		,nomb_fonet
  		,ape1_fonet
  		,ape2_fonet
  		,numtis_antiguo
  		,numtis_nuevo
	) values (
		oldmov.numerohc
		,current year to second
		,oldmov.nombre
		,oldmov.apellid1
		,oldmov.apellid2
		,oldmov.nomb_fonet
		,oldmov.ape1_fonet
		,oldmov.ape2_fonet
		,oldmov.numtis
		,''
	)
  );
/
