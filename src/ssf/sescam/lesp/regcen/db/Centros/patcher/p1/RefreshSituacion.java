package ssf.sescam.lesp.regcen.db.Centros.patcher.p1;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Hashtable;

import ssf.sescam.lesp.regcen.db.patcher.ClassPatcher;
import org.apache.log4j.Logger;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 Soluziona</p>
 * <p>Company: Soluziona</p>
 * @author not attributable
 * @version 1.0
 */

public class RefreshSituacion
    extends ClassPatcher {

  public boolean isDoubleConnectionRequired() {
    return true;
  }

  private static final String updateLesp =
      "Update lesp set spac=? where centro=? and norden=?";
  /**
   * Normalmente situacion=1. Seleccionamos los que no tienen situacion=1 y los refrescamos
   * y al resto le ponemos 1 (aquellos registros que tengan 'D','S','Q' o 'E'
   */
  private static final String selectListaEspe =
      "select norden,situacion from lista_espe where situacion<>1" +
      " union all " +
      "select nfila,situacion from h_lespadm where situacion<>1 ";

  private static final String updateLesp2 =
      "Update lesp set spac=1 where centro=? and spac in ('D','S','Q','E')";

  public boolean patchDoubleConnection(Hashtable params) {
    int CEGA = 0;
    Connection toSescam = null;
    Connection toCentro = null;

    CEGA = ( (Integer) params.get("CEGA")).intValue();
    toSescam = (Connection) params.get("toSescam");
    toCentro = (Connection) params.get("toCentro");
    Logger log=Logger.getLogger(getClass());
    log.info("Parche 1: Actualizando spac");

    try {
      PreparedStatement pstSescam = toSescam.prepareStatement(updateLesp);
      Statement stCentro = toCentro.createStatement();
      ResultSet rsMain = stCentro.executeQuery(selectListaEspe);
      float actualReg = 0;
      while (rsMain.next()) {
        actualReg++;
        if ( ( (float) actualReg) % (float) 1000 == (float) 0) {
          log.info("Parche 1: Procesados " + actualReg + " registros (Patch sit_paciente)");
        }
        pstSescam.setString(1, rsMain.getString(2));
        pstSescam.setInt(2, CEGA);
        pstSescam.setString(3, rsMain.getString(1));
        pstSescam.execute();
      }
      //Finalmente se coloca a 1 el resto
      PreparedStatement pstSescam2 = toSescam.prepareStatement(updateLesp2);
      pstSescam2.setInt(1, CEGA);
      pstSescam2.execute();
    }
    catch (SQLException ex) {
      log.error("Parche 1: Mientras se actualizaba el campo spac...",ex);
      return false;
    }
    return true;
  }
}