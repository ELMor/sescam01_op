insert into pacientes_log 
  	( 	numerohc
  		,fecope
  		,nombre
  		,apellid1
  		,apellid2
  		,nomb_fonet
  		,ape1_fonet
  		,ape2_fonet
  		,numtis_antiguo
  		,numtis_nuevo
	) select 
		numerohc
  		,current year to second
  		,nombre
  		,apellid1
  		,apellid2
  		,nomb_fonet
  		,ape1_fonet
  		,ape2_fonet
  		,''
  		,numtis
  	from pacientes;	
	
/
