package ssf.sescam.lesp.regcen.db.Centros.patcher.motrechazo;

import ssf.sescam.lesp.regcen.db.patcher.*;
import java.util.Hashtable;
import java.sql.Connection;
import org.apache.log4j.Logger;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import ssf.utils.Global;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 Soluziona</p>
 * <p>Company: Soluziona</p>
 * @author not attributable
 * @version 1.0
 */

public class Patch
    extends ClassPatcher {
  public Patch() {
  }

  public boolean isDoubleConnectionRequired() {
    return true;
  }

  public boolean patchDoubleConnection(Hashtable params) {
    boolean ret = true;
    int CEGA = 0;
    Connection toSescam = null;
    Connection toCentro = null;

    CEGA = ( (Integer) params.get("CEGA")).intValue();
    toSescam = (Connection) params.get("toSescam");
    toCentro = (Connection) params.get("toCentro");
    Logger log = Logger.getLogger(getClass());

    try {
      String sqlSescam = "update lesp set motrechazo=?, fecrechazo = ? where centro=" +
          CEGA + " and norden=?";
      PreparedStatement pst = toSescam.prepareStatement(sqlSescam);

      Statement st = toCentro.createStatement();

      try {
        st.execute("Drop view vlesp");
      }
      catch (SQLException ex) {
      }
      try {
        st.execute("Drop view vhlesp");
      }
      catch (SQLException ex1) {
      }
      
      String cView;
      cView = Global.getContentOfFile(
          "ssf/sescam/lesp/regcen/db/Centros/do/08.sql");
      st.execute(cView);
      cView = Global.getContentOfFile(
          "ssf/sescam/lesp/regcen/db/Centros/do/09.sql");
      st.execute(cView);
      
      String sql =
          "Select norden,motrechazo,fecrechazo " +
          "from lista_espe " +
          "union all " +
          "Select nfila,motrechazo,fecrechazo  " +
          "from h_lespadm";
      ResultSet rs = st.executeQuery(sql);

      float numReg = 0;
      while (rs.next()) {
        numReg++;
		if ( (numReg % 1000) == 0) {
               log.info("Refrescados motrechazo de " + numReg + " registros");
        }
        
        pst.setInt(1,rs.getInt(2));
        pst.setDate(2,rs.getDate(3));
        pst.setLong(3,rs.getLong(1));
        pst.execute();
      }
      rs.close();

    }
    catch (Exception e) {
      log.error("Ha fallado la actualizacion de motrechazo", e);
      ret = false;
    }
    return ret;
  }

}